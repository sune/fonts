/*
    Copyright (c) 2011 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE
*/

#include "fontmodel.h"

#include <QFontDatabase>
#include <QSize>

FontModel::FontModel(QObject *parent) :
  QAbstractTableModel(parent) {
  QFontDatabase db;
  m_database = db.families();
}

void FontModel::changeString(QString newstring) {
   m_string = newstring;
   dataChanged(createIndex(0,0),createIndex(0,rowCount(QModelIndex())));
}

int FontModel::rowCount(const QModelIndex& /*parent*/) const {
  return m_database.size();
}

int FontModel::columnCount(const QModelIndex& /*parent*/) const {
  return 2;
}

QVariant FontModel::data(const QModelIndex &index, int role) const {
   Q_ASSERT(index.column() < 2);
   if(role==Qt::DisplayRole) {
     if(index.column()==0) {
       return m_string;
     } else if(index.column()==1) {
       return m_database.at(index.row());
     }
   }
   if(role==Qt::FontRole && index.column()==0) {
     QString fontname = m_database.at(index.row());
     QHash<QString,QFont>::const_iterator it = m_fontcache.find(fontname);
     if(it ==m_fontcache.end()) {
       QFont f(fontname);
       f.setPointSize(30);
       m_fontcache.insert(fontname,f);
       return f;
     } else {
       return *it;
     }
   }
   if(role==Qt::SizeHintRole && index.column()==0) {
     return QSize(500,60);
   }
   return QVariant();
}
