/*
    Copyright (c) 2011 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE
*/

#include "gui.h"

#include <QTableView>
#include <QHeaderView>
#include <QLabel>
#include <QBoxLayout>
#include <QLineEdit>
#include <QPushButton>

#include "fontmodel.h"

Gui::Gui(QWidget *parent) :
  QWidget(parent), m_view(new QTableView(this)), m_edit(new QLineEdit(this)), m_fm(new FontModel) {
  /*Just to be 'pretty' - maybe replace with some real artwork one day*/
  QLabel* label = new QLabel("Font viewing application");
  label->setAlignment(Qt::AlignCenter);
  label->setMinimumHeight(100);

  QVBoxLayout* mainlay = new QVBoxLayout();
  mainlay->addWidget(label);

  {
    QHBoxLayout* lay = new QHBoxLayout();
    lay->addWidget(m_edit);
    mainlay->addLayout(lay);
  }

  connect(m_edit,SIGNAL(editingFinished()),this,SLOT(updateSamples()));

  m_view->horizontalHeader()->hide();
  m_view->verticalHeader()->hide();
  m_view->setModel(m_fm);
  m_view->resizeRowsToContents();
  m_view->resizeColumnsToContents();

  mainlay->addWidget(m_view);

  setLayout(mainlay);
}

Gui::~Gui() {
  delete m_fm;
}

void Gui::updateSamples() {
  m_fm->changeString(m_edit->text());
}
